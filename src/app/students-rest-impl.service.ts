import { Injectable } from '@angular/core';
import { StudentService } from './service/student-service';
import { Observable } from 'rxjs';
import Student from './entity/student';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentsRestImplService extends StudentService {
  saveStudent(student: Student): Observable<Student> {
    return this.http.post<Student>(environment.studentApi, student)
  }
  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(environment.studentApi);
  }

  getStudent(id: number): Observable<Student> {
    return this.http.get<Student>(environment.studentApi + '/' + id);
  }

  constructor(private http: HttpClient) {
    super();
  }
}
