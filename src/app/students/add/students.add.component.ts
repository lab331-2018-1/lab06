import { Component } from '@angular/core';
import Student from '../../entity/student';
import { StudentService } from '../../service/student-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-students-add',
  templateUrl: './students.add.component.html',
  styleUrls: ['./students.add.component.css']
})
export class StudentsAddComponent {
  constructor(private studentService: StudentService, private router: Router){
  }
  model: Student = new Student();
  onSubmit(){
    this.studentService.saveStudent(this.model).subscribe((student) => {
      this.router.navigate(['/detail', student.id]);
    }, (error) => {
      alert('could not save value');
    });
  }
  
  get diagnostic() {
    return JSON.stringify(this.model);
  }
  upQuantity(student: Student) {
    student.penAmount++;
  }

  downQuantity(student: Student) {
    if (student.penAmount > 0) {
      student.penAmount--;
    }
  }

}
